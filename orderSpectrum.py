#!/Users/juncheng/Desktop/Ureka/variants/common/bin/python
"""
  @package 
  @file orderSpectrum.py
  @seperate the orders, and plot the spectrum for each orders
 
  @brief Created by:
  @author Jun Cheng (Purdue)
 
  @warning This code is not fully validated
  and not ready for full release.  Please
  treat results with caution.

Usage: python orderSpectrum.py image.fits

Notes: 1.

"""

import matplotlib.pyplot as plt
import pyregion
import numpy as np
import sys
from astropy.io import fits
from astropy.nddata import NDData

#region1 = """
#image
#polygon(2295,150.5,2318,155,2340,157.5,2436,158,2718,161.5,2922,164,3143.5,168,3328.5,174.5,3649,190,3904,218.5,3994,235,3996,191,3942.5,188.5,3808.5,182,3578.5,172,3293.5,158.5,3099,155,2879.5,152.5,2662,148.5,2382.5,147.5,2337,146.5,2317,146.5)
#"""

#region2 = """
#image
#polygon(1422.75,169,1441.25,169.75,1748,166,1821.75,166.25,1941,165.75,2055.5,166,2263.75,168,2465,171.5,2661.75,176.25,2778.25,180.25,2849.25,183.5,3020,192.5,3187,205.75,3324.25,220.5,3386.25,228.25,3400.75,186.25,3348,185.75,3256.75,183.5,3137.5,178.5,2977,173.75,2816.5,170.5,2697.25,167.75,2483.5,165.25,2410.75,165,2191,163,2018,163,1844.5,163,1790,162.75,1634.75,163.5,1421,165)
#"""

#region3 = """
#image
#polygon(2747.75,216,2758.5,215.75,2763.25,189.5,2539.25,182.75,2347,179.25,2135,176.75,1844,174.5,1678.75,174.5,1183.75,177.75,1148.75,177.5,1078.75,179.25,1079.25,179.25,1080.5,179,1082.5,180.25,1083.5,181.25,1083.5,181.75,1321.5,181.75,1398.75,179.5,1513,181.5,1699.75,181.5,1995,184,2192.5,191,2415.5,196.75,2547.25,201.5)
#"""


region1="""
image
polygon(2702.8958,3971.6162,2704.4162,3971.6162,2704.4162,3970.0958,2702.8958,3970.0958)
polygon(2678.3388,4002.7396,2701.5824,4011.8096,2772.0848,3976.5584,2724.392,3816.8912,2695.3616,3514.1456,2672.552,3095.2784,2668.19\
44,2905.2083,2661.8333,2507.4861,2660.1803,2352.7647,2660.7398,2236.2302,2661.0161,2098.0423,2664.2283,1967.4853,2659.577,1946.7868,2\
659.3444,1982.3697,2655.6671,2097.5772,2654.9256,2192.2749,2649.2496,2316.7166,2644.4722,2429.0139,2647.3657,2657.4282,2647.3611,2938\
.5417,2653.8896,3358.6256,2682.92,3903.9824)
"""

region2="""
image
polygon(2702.8958,3971.6162,2704.4162,3971.6162,2704.4162,3970.0958,2702.8958,3970.0958)
polygon(2709.2876,3410.7698,2715.8528,3410.7698,2744.8812,3404.1115,2719.804,3235.3229,2696.1736,2956.8993,2681.8773,2637.2812,2672.7\
145,2275.3962,2669.9606,2029.9055,2670.9252,1962.39,2671.2361,1740.1871,2672.6829,1614.8013,2675.7477,1344.3731,2676.2299,1176.039,26\
78.6412,1076.2126,2673.3364,1082.4819,2669.4784,1266.7303,2667.5494,1386.3291,2665.4491,1641.3252,2662.7269,1937.3129,2664.1736,2114.\
2998,2664.5162,2349.1809,2665.963,2651.2666,2670.6142,2966.5444,2678.8125,3208.799,2689.4221,3357.8152,2692.3156,3418.0968)
"""

region3="""
image
polygon(2702.8958,3971.6162,2704.4162,3971.6162,2704.4162,3970.0958,2702.8958,3970.0958)
polygon(2690.2469,2781.7542,2709.537,2781.7542,2732.2029,2769.2157,2710.5015,2585.9595,2706.8148,2469.2303,2696.6875,2259.9325,2692.3\
472,2092.5579,2686.5602,1913.1597,2687.1389,1766.7199,2687.7176,1484.2569,2690.6111,1342.4745,2694.8333,1072.0208,2703.5139,767.00231\
,2712.9306,556.65509,2719.2963,450.17361,2722.25,201.70833,2727.8056,33.180556,2690.3056,17.208333,2688.9167,226.70833,2681.6806,446.\
70139,2673.4213,713.76157,2676.8935,856.70139,2679.787,1099.7986,2676.7222,1392.2431,2674.9861,1648.0301,2674.4074,1805.4931,2675.564\
8,1935.7292,2678.3619,2206.8846,2679.3264,2388.2118,2681.5664,2520.8553,2686.3889,2698.8067,2688.8002,2729.1887,2690.2469,2762.4641)
"""



#spectrumFile = sys.argv[1]

#def getRegion(regionFile1, regionFile2, regionFile3):

#    region5 = open(regionFile1, 'r').read()

def getMask(region1, region2, region3, matrix):
    r1 = pyregion.parse(region1)
    mask_1 = r1.get_mask(shape=(Xrange,Yrange))
    r2 = pyregion.parse(region2)
    mask_2 = r2.get_mask(shape=(Xrange,Yrange))
    r3 = pyregion.parse(region3)
    mask_3 = r3.get_mask(shape=(Xrange,Yrange))
    order1_matrix = np.zeros((Xrange,Yrange))
    order2_matrix = np.zeros((Xrange,Yrange))
    order3_matrix = np.zeros((Xrange,Yrange))
    
    for i in range(0,matrix.shape[0]):
        for j in range(0,matrix.shape[1]):
            if(mask_1[i,j]==True):
                order1_matrix[i,j]=matrix[i,j]
            if(mask_2[i,j]==True):
                order2_matrix[i,j]=matrix[i,j]
            if(mask_3[i,j]==True):
                order3_matrix[i,j]=matrix[i,j]
    
    return order1_matrix, order2_matrix, order3_matrix


def readin(fileName):
    hdulist = fits.open(fileName)
    matrix = hdulist[0].data
    Xrange = matrix.shape[0]
    Yrange = matrix.shape[1]
    return matrix, Xrange, Yrange

def pix2wavelength(a, b, n):
    wavelength.append(a*i+b)
    return wavelength


def spectrumPlot(order1_matrix, order2_matrix, order3_matrix):
    order1_spectrum = np.sum(order1_matrix, axis=1)
    order2_spectrum = np.sum(order2_matrix, axis=1)
    order3_spectrum = np.sum(order3_matrix, axis=1)
    
    yy_1 = []
    yy_2 = []
    yy_3 = []
    
    xx=[]
    xx_1 = []
    xx_2 = []
    xx_3 = []
    
    #print order1_spectrum.shape
    for i in range(0,order1_spectrum.shape[0]):
        yy_1.append(order1_spectrum[i])
        yy_2.append(order2_spectrum[i])
        yy_3.append(order3_spectrum[i])
        
        xx.append(i)
        xx_1.append(-0.4695*i + 2174.1)
        xx_2.append(-0.24*i + 1103.4)
        xx_3.append(-0.185*i + 764.26)
    
    writeSpectrum("order1.txt", xx_1, yy_1)
    writeSpectrum("order2.txt", xx_2, yy_2)
    writeSpectrum("order3.txt", xx_3, yy_3)
    

    plt.subplot(121).plot(xx,yy_1,'r',xx,yy_2,'b',xx,yy_3,'g' )
    plt.title('Spectrum(Red= 1st order, Blue= 2nd order, Green= 3rd order)')
    plt.xlabel('Pixel')
    plt.ylabel('Counts')
    plt.xlim([1000,4500])
    
    plt.subplot(122).plot(xx_1,yy_1,'r',xx_2,yy_2,'b',xx_3,yy_3,'g' )
    plt.title('Spectrum(Red= 1st order, Blue= 2nd order, Green= 3rd order)')
    plt.xlabel('Wavelength(nm)')
    plt.ylabel('Counts')
    plt.xlim([200,1200])
    plt.show()
    
def writeSpectrum(filename, wavelength, intensity): 
    outfile = open(filename,'w')
    for i in range(len(wavelength)):
        outfile.write(str(wavelength[i])+"\t"+str(int(intensity[i])) + "\n") 

    outfile.close()

# Write out fits images for each order
def writeoutOrders(order1, order2, order3):
    hdu1=fits.PrimaryHDU(order1)
    hdu2=fits.PrimaryHDU(order2)
    hdu3=fits.PrimaryHDU(order3)
    
    hdu1.writeto('order1.fits',clobber='true')
    hdu2.writeto('order2.fits',clobber='true')
    hdu3.writeto('order3.fits',clobber='true')

fileName = sys.argv[1]
matrix,Xrange,Yrange = readin(fileName)
order1_matrix, order2_matrix, order3_matrix = getMask(region1, region2, region3, matrix)
writeoutOrders(order1_matrix, order2_matrix, order3_matrix)
spectrumPlot(order1_matrix, order2_matrix, order3_matrix)

print np.sum(order1_matrix)
print np.sum(order2_matrix)
print np.sum(order3_matrix)

